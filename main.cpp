#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <fstream>
using namespace std;
struct Token
{
	string tok;
	string type;
	int length;
	int column;
	int line;	
};
class lexical {
	static const int keywordsCount = 48;
	static const int punctuationCount = 20;
	static const int operatorsCount = 11;
private:
	char keywords[keywordsCount][10] = {
		"bool","break","case","catch","char","class","const","continue","default",
		"delete","do","double","else","enum","false","float","for","unsigned","using",
		"friend","if","inline","int","long","namespace","virtual","new","operator","private",
		"protected","public","register","void","return","short","signed","sizeof","static","struct",
		"switch","template","this","throw","true","try","typedef","unsigned","while"
	};	
	char punctuation[punctuationCount] = {
		'!','"','#','$','%','\'','(',')',',','.',':',';','?','@','[',']','`','{','}','~'
	};
	char operators[operatorsCount] = {
		'+','-','/','*','%','>','<','^','=','&','|'
	};
	int LineCounter = 1;
	int ColumnCounter = 1;
	int tokenIndex = 0;
public:
	vector <Token> Tokens;
	bool isKeyWord(string token) {
		for (int i = 0; i < keywordsCount; i++) {
			if (token == keywords[i]) {
				return true;
			}
		}
		return false;
	}
	bool isPunctuation(char symbol) {
		for (int i = 0; i < punctuationCount; i++) {
			if (symbol == punctuation[i]) {
				return true;
			}
		}
		return false;
	}
	bool isOperator(char symbol) {
		for (int i = 0; i < operatorsCount; i++) {
			if (symbol == operators[i]) {
				return true;
			}
		}
		return false;
	}
	bool isLetter(char symbol) {
		if ((symbol >= 'a' && symbol <= 'z') || (symbol >= 'A' && symbol <= 'Z')) {
			return true;
		}
		return 0;
	}
	bool isNumber(char symbol) {
		if (symbol >= '0' && symbol <= '9') {
			return true;
		}
		return false;
	}
	bool isWhiteSpace(char symbol) {
		if (symbol == ' ') {
			return true;
		}
		return false;
	}
	char getNextSymbol(FILE * inputFile) {
		char sym = (char)getc(inputFile);
		ColumnCounter++;		
		return sym;
	}	
	bool Analyzer(FILE * inputFile) {
		char Symbol = getNextSymbol(inputFile);		
		while (Symbol != EOF) {
			char Buffer[1000];
			int BufferCounter = 0;
			Buffer[BufferCounter++] = Symbol;
			Buffer[BufferCounter] = NULL;
			Token tok;			
			int tokenLine = LineCounter;
			int tokenColumn = ColumnCounter-1;			
			tok.line = tokenLine;
			tok.column = tokenColumn;
			if(isLetter(Symbol) || Symbol == '_') {						
				char nextSymbol = getNextSymbol(inputFile);
				while (nextSymbol != EOF && !isPunctuation(nextSymbol) && !isWhiteSpace(nextSymbol)&& !isOperator(nextSymbol))
				{
					if (nextSymbol == '\n') {																		
						break;
					}
					Buffer[BufferCounter++] = nextSymbol;
					Buffer[BufferCounter] = NULL;
					nextSymbol = getNextSymbol(inputFile);
				}
				ungetc(nextSymbol, inputFile);	
				ColumnCounter--;		
				tok.tok = Buffer;
				tok.length = BufferCounter;								
				if (isKeyWord(Buffer)) {					
					tok.type = "Keyword";
				}
				else tok.type = "Identifier";				
				Tokens.push_back(tok);
			}
			else if (isOperator(Symbol)) {
				char nextSymbol = getNextSymbol(inputFile);
				if (nextSymbol != EOF) {
					if (isOperator(nextSymbol)) {					
						if ((nextSymbol == '=') || ((Symbol == '+' || Symbol == '-' || Symbol == '>' || Symbol == '<') && Symbol == nextSymbol) || (Symbol=='-' && nextSymbol=='>')) {
							Buffer[BufferCounter++] = nextSymbol;
							Buffer[BufferCounter] = NULL;
							tok.tok = Buffer;
							tok.length = BufferCounter;
							tok.type = "Operator";							
							Tokens.push_back(tok);
						}
						else if ((Symbol == '&' && nextSymbol == '&') || (Symbol == '|' && nextSymbol == '|')) {
							Buffer[BufferCounter++] = nextSymbol;
							Buffer[BufferCounter] = NULL;
							tok.tok = Buffer;
							tok.length = BufferCounter;
							tok.type = "Operator";
							Tokens.push_back(tok);
						}
						else if (Symbol =='/' && nextSymbol =='/') {
							while (nextSymbol != EOF && nextSymbol != '\n')
							{
								nextSymbol = getNextSymbol(inputFile);
							}
							ungetc(nextSymbol, inputFile);							
							ColumnCounter--;
						}
						else if (Symbol == '/' && nextSymbol == '*') {
							while (nextSymbol != EOF)
							{
								nextSymbol = getNextSymbol(inputFile);
								if (nextSymbol == '*') {
									char next = getNextSymbol(inputFile);
									if (next == '/') break;
									else nextSymbol = next;
								}
							}
						}
					}
					else {
						tok.tok = Buffer;
						tok.length = BufferCounter;
						tok.type = "Operator";
						Tokens.push_back(tok);
						ungetc(nextSymbol, inputFile);						
						ColumnCounter--;
					}
				}
			}
			else if (isNumber(Symbol)) {
				char nextSymbol = getNextSymbol(inputFile);
				int isDouble = 0;
				while (nextSymbol != EOF) {
					if (isNumber(nextSymbol)) {
						Buffer[BufferCounter++] = nextSymbol;
						Buffer[BufferCounter] = NULL;
					}
					else if (nextSymbol == '.') {
						char next = getNextSymbol(inputFile);
						if (isNumber(next)) {
							Buffer[BufferCounter++] = nextSymbol;
							Buffer[BufferCounter++] = next;
							Buffer[BufferCounter] = NULL;		
							isDouble = 1;
						}
						else {
							ungetc(next, inputFile);
							ColumnCounter--;
							ungetc(nextSymbol, inputFile);
							ColumnCounter--;
							break;
						}						
					}			
					else{
						ungetc(nextSymbol, inputFile);
						ColumnCounter--;
						break;
					}
					nextSymbol = getNextSymbol(inputFile);
				}
				tok.tok = Buffer;
				tok.length = BufferCounter;
				if (isDouble)
					tok.type = "Number(Double)";
				else tok.type = "Number(Integer)";				
				Tokens.push_back(tok);
			}			
			else if (isPunctuation(Symbol)) {
				char nextSymbol = getNextSymbol(inputFile);
				int isString = 0;
				int isChar = 0;
				int isOperator = 0;
				Token Pun;
				if (Symbol == '"') {	
					Pun.tok = "\"";
					Pun.length = 1;
					Pun.line = tokenLine;
					Pun.column = tokenColumn;
					Pun.type = "Punctutaion";
					Tokens.push_back(Pun);
					tok.column++;
					tokenColumn++;
					BufferCounter--;
					while (nextSymbol != EOF && nextSymbol != '"') {
						if(nextSymbol != '\n'){
							if (nextSymbol == '\\') {
								nextSymbol = getNextSymbol(inputFile);
								ColumnCounter--;
							}
							Buffer[BufferCounter++] = nextSymbol;
							Buffer[BufferCounter] = NULL;
						}						
						nextSymbol = getNextSymbol(inputFile);
					}					
					isString = 1;
				}				
				else if (Symbol == '\'') {
					Pun.tok = "'";
					Pun.length = 1;
					Pun.line = tokenLine;
					Pun.column = tokenColumn;
					Pun.type = "Punctutaion";
					Tokens.push_back(Pun);
					tokenColumn++;
					tok.column++;
					BufferCounter--;
					while (nextSymbol != EOF && nextSymbol != '\'') {
						if (nextSymbol != '\n') {
							if (nextSymbol == '\\') {
								nextSymbol = getNextSymbol(inputFile);
								ColumnCounter--;
							}
							Buffer[BufferCounter++] = nextSymbol;
							Buffer[BufferCounter] = NULL;
						}
						nextSymbol = getNextSymbol(inputFile);
					}		
					isChar = 1;
				}
				else if (Symbol == '!') {					
					if (nextSymbol == '=') {
						Buffer[BufferCounter++] = nextSymbol;
						Buffer[BufferCounter] = NULL;
						isOperator = 1;
					}
					else {
						ungetc(nextSymbol, inputFile);
						ColumnCounter--;
					}
				}
				else {
					ungetc(nextSymbol, inputFile);
					ColumnCounter--;
				}
				tok.tok = Buffer;
				tok.length = BufferCounter;
				if (isString)
					tok.type = "String";					
				else if (isChar)
					tok.type = "Char";					
				else if (isOperator)
					tok.type = "Operator";					
				else
					tok.type = "Punctutaion";		
				Tokens.push_back(tok);	
				if (isChar) {
					Pun.tok = "'";
					Pun.length = 1;
					Pun.line = LineCounter;
					Pun.column = ColumnCounter-1;
					Pun.type = "Punctutaion";
					Tokens.push_back(Pun);					
				}
				else if (isString) {
					Pun.tok = "\"";
					Pun.length = 1;
					Pun.line = LineCounter;
					Pun.column = ColumnCounter-1;
					Pun.type = "Punctutaion";
					Tokens.push_back(Pun);
				}
			}
			else if (Symbol == '\t') ColumnCounter += 3;			
			else if (Symbol == '\n') {
				LineCounter++;
				ColumnCounter = 1;
			}
			Symbol = getNextSymbol(inputFile);
		}				
		return true;
	}

	Token getNextToken(){
		if (tokenIndex < Tokens.size())
			return Tokens[tokenIndex++];
		else{
                    cout<<"Invalid"<<endl;
                    exit(1);
                }
	}
};
class Parser{
private:
	lexical lex;
	Token lookahead;
        int LabelNum = 0;
public:
        FILE * CodeGenerated = fopen("CodeGenerated.txt","w");
	Parser(FILE * inputFile){
		lex.Analyzer(inputFile);
		lookahead = lex.getNextToken();
	}	                        
        bool caseFunction(int breakLabel){
            if(lookahead.tok == "}"){
                return true;
            }
            if(lookahead.tok == "case"){
                lookahead = lex.getNextToken();
                if(lookahead.type == "Number(Integer)"){
                    fprintf(CodeGenerated,"push %s\n==\n",lookahead.tok.c_str());  
                    int falseLabel = LabelNum++;
                    fprintf(CodeGenerated,"goFalse L%d\n",falseLabel);
                    lookahead = lex.getNextToken();
                    if(lookahead.tok == ":"){
                        lookahead = lex.getNextToken();
                        if(stmtFunction()){
                            if(lookahead.tok == ";"){
                                lookahead = lex.getNextToken();
                                if(lookahead.tok == "break"){
                                    lookahead = lex.getNextToken();
                                    if(lookahead.tok == ";"){
                                        lookahead = lex.getNextToken();
                                        fprintf(CodeGenerated,"Label L%d\n",falseLabel);
                                        fprintf(CodeGenerated,"goTo L%d\n",breakLabel);
                                        if(caseFunction(breakLabel)){
                                            return true;    
                                        }
                                        else return false;
                                    }
                                    else return false;
                                }       
                                else return false;
                            }
                            else return false;                            
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }
        bool switchFucntion(){
            if(lookahead.tok == "switch"){
                lookahead = lex.getNextToken();
                if(lookahead.tok == "("){
                    lookahead = lex.getNextToken();
                    if(lookahead.type == "Identifier"){
                        int breakLabel = LabelNum++;
                        fprintf(CodeGenerated,"Rvalue %s\n",lookahead.tok.c_str());
                        lookahead = lex.getNextToken();
                        if(lookahead.tok == ")"){
                            lookahead = lex.getNextToken();
                            if(lookahead.tok == "{")
                            {
                                lookahead = lex.getNextToken();
                                if(caseFunction(breakLabel)){
                                    fprintf(CodeGenerated,"Label L%d\n",breakLabel);
                                    if(lookahead.tok == "}"){
                                        lookahead = lex.getNextToken();
                                        return true;
                                    }
                                    else return false;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        }        
	bool programFunction(){
		if (lookahead.tok == "main"){
			lookahead = lex.getNextToken();
			if (lookahead.tok == "(") {
				lookahead = lex.getNextToken();
				if (lookahead.tok == ")") {
					lookahead = lex.getNextToken();
					if(compoundStmtFunction()){
                                            fprintf(CodeGenerated,"HALT\n");
                                            return true;
                                        }
					else return false;
				}
				else return false;
			}
			else return false;
		}
		else return false;
	}	        
	bool stmtFunction() {	
		if (lookahead.tok == "while") {			
                    if (whileFunction()) {				
			return true;
                    }
                    else return false;
		}
		else if (lookahead.tok == "{") {
                    if (compoundStmtFunction()) {				
			return true;
                    }
                    else return false;
		}
		else if (lookahead.tok == "do") {
                    if (doFunction()) {
			return true;
                    }
                    else return false;
		}
                else if(lookahead.tok == "if"){
                    if(ifFuntion()){
                        return true;
                    }             
                    else return false;
                }
                else if (lookahead.tok == "for"){
                    if(forFunction()){
                        return true;
                    }
                    else return false;
                }
                else if(lookahead.type == "Identifier"){
                    string tt = "";
                    if(assignmentFunction(&tt)){
                        fprintf(CodeGenerated,"%s",tt.c_str());
                        return true;
                    }
                    else return false;
                }
                else if(lookahead.tok == "switch"){
                    if(switchFucntion()){
                        return true;
                    }
                    else return false;
                }
		else return true;
	}                    
	bool doFunction(){
            if (lookahead.tok == "do") {
                int doLabel = LabelNum++;
                fprintf(CodeGenerated,"Label L%d\n",doLabel);
                lookahead = lex.getNextToken();
                if(stmtFunction()){
                    if (lookahead.tok == "while") {
                        if (whileFunction()) {
                            fprintf(CodeGenerated,"goTo L%d\n",doLabel);
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
	}                
	bool whileFunction() {
            string CondtionCode = "";
            if (lookahead.tok == "while") {
		lookahead = lex.getNextToken();
		if (lookahead.tok == "(") {
                    lookahead = lex.getNextToken();    
                    int condLabel = LabelNum++;
                    if(condtionFunction(&CondtionCode)){
                        fprintf(CodeGenerated,"%s",CondtionCode.c_str());
                        fprintf(CodeGenerated,"goFalse L%d\n",condLabel);
                        if (lookahead.tok == ")") {
                            lookahead = lex.getNextToken();	
                            if (stmtFunction()) {
                                fprintf(CodeGenerated,"Label L%d\n",condLabel);
                                return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;                
            }
            else return false;
	}        
        bool termFunction(string * Code){
            if(factorFunction(Code)){
                if(termPrimeFunction(Code)){
                    return true;
                }
                else return false;             
            }
            else return false;
        }        
        bool termPrimeFunction(string * Code){
            if(lookahead.tok == "*" || lookahead.tok == "/" || lookahead.tok == "%"){
                Token savedTok = lookahead;
                lookahead = lex.getNextToken();                
                if(factorFunction(Code)){
                    char temp[1000];
                    sprintf(temp,"%s\n",savedTok.tok.c_str());
                    *Code += temp;
                    if(termPrimeFunction(Code)){
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return true;
        }
        bool factorFunction(string * Code){
            if(lookahead.type == "Identifier" || lookahead.type == "Number(Integer)" || lookahead.type == "Number(Double)"){
                char temp[1000];
                if(lookahead.type == "Identifier"){
                    sprintf(temp,"Rvalue %s\n",lookahead.tok.c_str());
                }
                else{
                    sprintf(temp,"Push %s\n",lookahead.tok.c_str());
                }
                *Code += temp;
                lookahead = lex.getNextToken();
                return true;
            }
            else if(lookahead.tok == "("){
                lookahead = lex.getNextToken();
                if(expFunction(Code)){
                    if(lookahead.tok == ")"){
                        lookahead = lex.getNextToken();
                        return true;
                    }
                    else return false;
                }
                else return false;                       
            }
            else return false;
        }
        bool expPrimFunction(string * Code){
            if(lookahead.tok == "+" || lookahead.tok == "-"){
                Token savedTok = lookahead;
                lookahead = lex.getNextToken();                
                if(termFunction(Code)){
                    char temp[1000];
                    sprintf(temp,"%s\n",savedTok.tok.c_str());
                    *Code += temp;
                    if(expPrimFunction(Code)){
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return true;
        }
        bool expFunction(string * Code){            
            if(termFunction(Code)){
                if(expPrimFunction(Code)){
                    return true;
                }
                else return false;
            }
            else return false;            
        }   
        bool assignmentFunction(string * Code){
            char temp[1000];
            if(lookahead.type == "Identifier"){
                sprintf(temp,"Lvalue %s\n",(lookahead.tok).c_str());
                *Code +=temp;
                lookahead = lex.getNextToken();
                if(lookahead.tok == "="){
                    lookahead = lex.getNextToken();
                    if(expFunction(Code)){
                        sprintf(temp,":=\n");
                        *Code +=temp;
                        if(lookahead.tok == ";" || lookahead.tok == ")" || lookahead.tok == ","){                            
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }            
            else return false;
        }		
        bool compoundStmtFunction() {
		if (lookahead.tok == "{") {
                    lookahead = lex.getNextToken();
                    if(lookahead.tok == "}"){
                        lookahead = lex.getNextToken();
                        return true;
                    }
                    if (stmtFunction()) {
                        if (lookahead.tok == ";") {
                            lookahead = lex.getNextToken();
                            if (moreStmtFunction()) {
                                if (lookahead.tok == "}") {
                                    lookahead = lex.getNextToken();
                                    return true;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else if(lookahead.tok == "}"){
                            lookahead = lex.getNextToken();
                            return true;
                        }
                        else if(stmtFunction()){
                            return true;
                        }
                        else return false;
                    }
                    else return false;
                }
		else return false;
	}
        bool ifFuntion(){
            string CondtionCode="";
            if(lookahead.tok == "if"){
                lookahead = lex.getNextToken();
                if(lookahead.tok == "("){
                    lookahead = lex.getNextToken();                                         
                    int condLabel = LabelNum++;
                    if(condtionFunction(&CondtionCode)){
                        fprintf(CodeGenerated,"%s",CondtionCode.c_str());
                        fprintf(CodeGenerated,"goFalse L%d\n",condLabel);
                        if(lookahead.tok == ")"){
                            lookahead = lex.getNextToken();
                            if(stmtFunction()){
                                if(lookahead.tok == ";"){
                                    lookahead = lex.getNextToken();
                                }
                                int goToLabel = LabelNum++;
                                fprintf(CodeGenerated,"goTo L%d\n",goToLabel);
                                fprintf(CodeGenerated,"Label L%d\n",condLabel);
                                if(lookahead.tok == "else"){
                                    lookahead = lex.getNextToken();
                                    if(stmtFunction()){
                                        fprintf(CodeGenerated,"Label L%d\n",goToLabel);
                                        return true;
                                    }
                                    else return false;
                                }
                                else return true;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
        } 
        bool simpleFunction(string * code){
            if(lookahead.type == "Identifier" || lookahead.type == "Number(Integer)" || lookahead.type == "Number(Double)"){
                char temp[1000];
                if(lookahead.type == "Identifier"){
                    sprintf(temp,"Rvalue %s\n",lookahead.tok.c_str());
                }
                else{
                    sprintf(temp,"%s\n",lookahead.tok.c_str());
                }
                *code += temp;
                lookahead = lex.getNextToken();
                return true;
            }
            else return false;
        }
        bool relationalFunction(string * code){
            if(lookahead.tok == "||" || lookahead.tok == "&&" || lookahead.tok == ">"||
               lookahead.tok == "<" || lookahead.tok == ">=" || lookahead.tok == "<="||
               lookahead.tok == "==" || lookahead.tok == "!="){
               char temp[1000];
               sprintf(temp,"%s\n",lookahead.tok.c_str());
               * code += temp;
               lookahead = lex.getNextToken();
               return true;
            }
            else return false;
        }
        bool condtionFunction(string * Code) {   
            string simple1Code="",simple2Code="",relationCode="";
            if(lookahead.tok == ";" || lookahead.tok == ")"){                
                return true;
            }
            if(simpleFunction(&simple1Code)){
                if(!lex.isOperator(lookahead.tok[0])){
                    *Code += simple1Code;
                    return true;
                }
                if(relationalFunction(&relationCode)){
                    if(simpleFunction(&simple2Code)){
                        *Code += simple1Code;
                        *Code += simple2Code;
                        *Code += relationCode;
                        return true;
                    }
                    else return false;
                }
                else return false;
            }
            else return false;
            
	}
	bool moreStmtFunction() {		
            if(lookahead.tok == "}" || lookahead.tok == ";"){                
                return true;               
            }
            if(stmtFunction()){
                lookahead = lex.getNextToken();
                if(moreStmtFunction()){
                    return true;
                }
                else return false;
            }
            else return false;
	}
        bool morePostListFunction(string * Code){
            if(lookahead.tok == ")"){
                lookahead = lex.getNextToken();
                return true;
            }
            if(postlistFunction(Code)){
                return true;
            }            
            else return false;
        }
        bool morePreListFunction(string * Code){
            if(lookahead.tok == ";"){
                lookahead = lex.getNextToken();
                return true;
            }
            if(prelistFunction(Code)){
                return true;
            }            
            else return false;
        }
        bool prelistFunction(string * Code){
            if(lookahead.tok == ";"){                
                return true;
            }
            else if(assignmentFunction(&*Code)){
                if(lookahead.tok == "," ){
                    lookahead = lex.getNextToken();
                    if(morePreListFunction(Code)){
                        return true;
                    }
                    else return false;
                }
                else if(lookahead.tok == ";"){                    
                    return true;
                }
                else return false;
            }
            else return false;
        }
        bool postlistFunction(string * Code){
            if(lookahead.tok == ")"){                
                return true;
            }
            else if(assignmentFunction(&*Code)){
                if(lookahead.tok == "," ){
                    lookahead = lex.getNextToken();
                    if(morePostListFunction(Code)){
                        return true;
                    }
                    else return false;
                }
                else if(lookahead.tok == ")"){                    
                    return true;
                }
                else return false;
            }
            else return false;
        }
        bool forFunction(){
            string CondtionCode = "",PreListCode = "",PostListCode ="",stntCode = "";
            if(lookahead.tok == "for"){
                lookahead = lex.getNextToken();
                if(lookahead.tok == "("){
                    lookahead = lex.getNextToken();
                    if(prelistFunction(&PreListCode)){
                        fprintf(CodeGenerated,"%s",PreListCode.c_str());
                        if(lookahead.tok == ";"){
                            lookahead = lex.getNextToken();
                            fprintf(CodeGenerated,"Label L%d\n",LabelNum);
                            int savedLabel = LabelNum++;
                            int condLabel = LabelNum++;
                            if(condtionFunction(&CondtionCode)){ 
                                fprintf(CodeGenerated,"%s",CondtionCode.c_str());
                                fprintf(CodeGenerated,"goFalse L%d\n",condLabel);
                                if(lookahead.tok == ";" || lookahead.tok == ")"){
                                    lookahead = lex.getNextToken();                               
                                    if(postlistFunction(&PostListCode)){
                                        if(lookahead.tok == ")"){
                                            lookahead = lex.getNextToken();
                                            if(stmtFunction()){
                                                fprintf(CodeGenerated,"%s",PostListCode.c_str());
                                                fprintf(CodeGenerated,"goTo L%d\n",savedLabel);
                                                fprintf(CodeGenerated,"Label L%d\n",condLabel);
                                                return true;
                                            }
                                            else return false;                                            
                                        }
                                        else return false;
                                    }
                                    else return false;
                                }
                                else return false;
                            }
                            else return false;
                        }
                        else return false;
                    }
                    else return false;
                }                    
                else return false;
            }
            else return false;
        }
};
int main() {
    char input[1024];
    cout << "Insert input file name" << endl;
    cin >> input;
    FILE * inputFile = fopen(input, "r+");
    if (!inputFile) {
	perror("Error");
        exit(0);
    }
    Parser parse(inputFile);
    if (parse.programFunction()) {
	cout << "Valid" << endl;
    }
    else {
	cout << "Invalid" << endl;
    }	
    fcloseall();
}